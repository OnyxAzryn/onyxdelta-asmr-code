#Basic iteration using for loops
def addOne(value):
    """Return the input plus one"""
    return value+1

def main():
    """Main program loop"""
    #for loop prints "Adding one." and call addOne on the iterator 5 times
    #for example
    for i in range(5):
        print("Adding one via addOne().")
        print(addOne(i))

    #Data structure iteration (applies to any multi-element data structure we have discussed)
    targetList = [0,1,2,3,4,5,6,7,8,9]
    #Two options for for loop iteration
    for i in targetList:
        #i is assigned to value on current iteration
        print(i)
    #Get length of targetList and pass that to range()
    for i in range(len(targetList)):
        #i is assigned to counting value for current iteration and then indexes into targetList
        print(targetList[i])

    #Iterate through list until 7 is reached
    for i in targetList:
        #Check if i is 7
        if i != 7:
            print("Not at seven yet.")
        else:
            print("At seven now.")
            #Abruptly end iteration
            break

    #Summing of list items until a condition is met
    currentSum = 0
    for i in targetList:
        if currentSum < 10:
            #Increment sum by i
            currentSum += i
        else:
            #Abruptly end iteration
            break
    print("The list sum was " + str(currentSum))

if __name__ == "__main__":
    #Enter main if program is run
    main()