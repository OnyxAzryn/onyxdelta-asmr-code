#Basic iteration using while loops
def addOne(value):
    """Return the input plus one"""
    return value+1

def main():
    """Main program loop"""
    #while loop prints "Adding one." and call addOne on the iterator 5 times
    x = 0
    while x < 5:
        print("Adding one via addOne().")
        print(addOne(x))
        x += 1

    #Data structure iteration (applies to any multi-element data structure we have discussed)
    targetList = [0,1,2,3,4,5,6,7,8,9]
    currentIndex = 0
    #One option for while loop iteration
    while currentIndex < len(targetList):
        #i is assigned to counting value for current iteration and then indexes into targetList
        print(targetList[currentIndex])
        #Increment iterator
        currentIndex += 1

    #Iterate through list until 7 is reached
    iterationStep1 = 0
    #Check two conditions using short-circut evaluation
    while iterationStep1 < len(targetList) and targetList[iterationStep1] != 7:
        print("Not at seven yet.")
        #Increment iterator
        iterationStep1 += 1
    print("At seven now.")

    #Summing of list items until a condition is met
    interationStep2 = 0
    currentSum = 0
    while interationStep2 < len(targetList) and currentSum < 10:
        #Increment currentSum by element
        currentSum += targetList[interationStep2]
        interationStep2 += 1
    print("The list sum was " + str(currentSum))

if __name__ == "__main__":
    #Enter main if program is run
    main()