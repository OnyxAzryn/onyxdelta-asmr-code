#This is a single line comment
"""This is a multiline comment.
We can type on multiple lines.
This will only end once we terminate it like so."""

exampleVariable = 1
exampleVariable = "Hello" #Reassignment of an variable containing an integer to a string
"""As you do not need to specifiy ahead of time what kind of variable exampleVariable is
(Integer or something else), this shows Python has inferred typing.

As you are allowed to reassign variables to different types, this is an example of
how Python is a dynamically typed langague."""

variable1 = 0 #Legal variable name
Variable = 0 #Another legal variable name
"""The following variable names are not legal:
1variable = 0, as it starts with a number
&var = 0, as it starts with a symbol"""
x = 0 #Legal variable name, but not descriptive as to what it does

#Basic Integer Operations
print("Integer operations on 21 and 2:")
firstNumber = 21
secondNumber = 2
print(firstNumber + secondNumber) #Addition: Prints 23
print(firstNumber - secondNumber) #Subtraction: Prints 19
print(firstNumber * secondNumber) #Multiplication: Prints 42
print(firstNumber / secondNumber) #Division: Prints 10.5 (Floating point number)
#More operations
print(firstNumber % secondNumber) #Modulo: Prints 1
print(firstNumber ** secondNumber) #Exponent: Prints 441
print(firstNumber // secondNumber) #Floor division: Prints 10

#Basic Float Operations
print("Float operations on 21.0 and 2.0:")
firstNumber = 21.0
secondNumber = 2.0
print(firstNumber + secondNumber) #Addition: Prints 23.0
print(firstNumber - secondNumber) #Subtraction: Prints 19.0
print(firstNumber * secondNumber) #Multiplication: Prints 42.0
print(firstNumber / secondNumber) #Division: Prints 10.5 (Floating point number)
#More operations
print(firstNumber % secondNumber) #Modulo: Prints 1.0
print(firstNumber ** secondNumber) #Exponent: Prints 441.0
print(firstNumber // secondNumber) #Floor division: Prints 10.0 (Because both arguments are floats)

#Concatenation
print("Concatenation on Hello and World!")
string1 = "Hello"
string2 = "World!"
print(string1 + string2) #Prints HelloWorld! (No space)
print(string1 - string2) #Not possible