#Episode six covering mechanics of functions and concepts of scoping
'''While writing this program, I realized I made an error in previous videos.  I previously
said that triple-double quotes (""") were used to make multi-line comments.  Although this
works, it violates Python's standards and should not be used.  From here on out, I will
surround multi-line comments with triple-single quotes for clarity (as seen in this
comment).  Instead, triple-double quotes should be used for function docstrings.
I sincerely apologize for this mistake.'''

globalVariable1 = 1

def absFunc(parameter1):
    """A function that takes in a single parameter and returns the absolute value of that parameter"""
    print("The value of globalVariable1 is " + str(globalVariable1) + " as reported from absFunc().")
    print("Running absFunc on " + str(parameter1))
    return abs(parameter1) #Call abs() on parameter1 and return value

def scopingExample():
    """A function to demonstrate where a certain variable is alive"""
    print("The value of globalVariable1 is " + str(globalVariable1) + " as reported from scopingExample().")
    x = "Help" #Define x to be the string Help
    return x

def globalIncrementGlobalVariable1():
    """Adds one to globalVariable1"""
    global globalVariable1 #Tells interpreter to use reference global variable
    globalVariable1 += 1 #Increment globalVariable1 by one

def localIncrementGlobalVariable1():
    """Attempts to add one to globalVariable1 without global keyword"""
    globalVariable1 += 1 #Invalid code, will not run as globalVariable1 is not defined in this scope

def main():
    """Main function of the program"""
    print("The value of globalVariable1 is " + str(globalVariable1) + " as reported from main().")
    x = absFunc(5) #Function called and ran here (eager evaluation)
    print(absFunc(-100)) #Functioned called, ran, and value printed here
    globalIncrementGlobalVariable1() #Increment globalVariable1
    print(scopingExample()) #scopingExample() called, ran, and value printed here
    print(x) #Print back the result from the call to absFunc(5) stored in x

if __name__ == "__main__":
    #If running the script, start here and call main()
    main()