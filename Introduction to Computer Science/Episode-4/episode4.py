#Example 1
user_input1 = input("Enter some text: ")
print(len(user_input1), type(user_input1))



#Example 2
user_input2 = int(input("Enter a number: "))
if user_input2 >= 5:
    print(str(user_input2) + " is greater than, or equal to, 5.")
else:
    print(str(user_input2) + " is less than 5.")



#Example 3
user_input3 = float(input("Enter another number: "))
if user_input3 > 0:
    print(str(user_input3) + " is greater than 0.")
elif user_input3 < 0:
    print(str(user_input3) + " is less than 0.")
else:
    print(str(user_input3) + " is exactly 0.")
