"""Basic program covering concepts from Episode 1 to Episode 4
These include:
- Variables
- Type casting
- User input
- Use of len()
- Single and multi-line comments
- Basic mathematical operations
- Conditionals based upon comparisions
- Printing information to the terminal
- Data structures"""

#Define target structures
checkTupleNumbers = (1, 2, 3, 4, 5, 6)
checkListStrings = ["String", "is", "at", "length"]
matchDictionary = {}

#Take in a real number
user_input1 = float(input("Enter any real number here: "))

#Determine even, odd, or neither
if user_input1 % 2 == 0:
    print("You entered a number divisible by two.  Thus, this number is even.")
else:
    print("You entered a number not divisible by two.")
    if user_input1 == int(user_input1):
        print("You have entered an odd number.")
    else:
        print("The number you enter is neither odd, nor even, as it is not an integer.")

#Determine if input is positive, negative, or zero.
if user_input1 > 0:
    print("You entered a number that is positive.")
elif user_input1 < 0:
    print("You entered a number that is negative.")
else:
    print("You entered the number zero.")

#Take in a string
user_input2 = input("Enter a string: ")

#Print back the string
print("Your string is " + str(len(user_input2)) + " characters long.")

#Determine if the number entered is greater than, less than, or equal to the length of the string and display by how much
length_difference = user_input1 - len(user_input2)
if length_difference < 0:
    print("Your number is less than the number of characters in the string by " + str(abs(length_difference)) + ".")
elif length_difference > 0:
    print("Your number is greater than the number of characters in the string by " + str(length_difference) + ".")
else:
    print("Your number is the same as the number of characters in the string.")

#Check for strings in list
if user_input2 in checkListStrings:
    print("Your string was found in checkListStrings: " + user_input2)
    #Check for numbers in tuple
    if user_input1 in checkTupleNumbers:
        print("Your number was found in checkTupleNumbers: " + str(user_input1))
        matchDictionary[user_input1] = user_input2
        print("The matching sequence is shown in dictionary as " + str(matchDictionary.items()) + ".")
    else:
        print("Your number was not found in checkTupleNumbers.")
else:
    print("Your string was not found in checkListStrings.")