#Introduction to data types, len(), and more errors
##Types from previous episodes
integerVar = 20
floatVar = 20.0
stringVar = "This is a string"
##New types
###Tuples
print("Tuples:")
tupleVar = (1, 2.0, "h") #Immutable and can contain heterogenous types
print(tupleVar[0]) #Prints 1
#tupleVar[0] = 1 is not possible
###Lists
print("Lists:")
listVar = ["a", "b", "c", 1, 2.0 , 3] #Mutable and can contain heterogenous types
print(listVar[0])
listVar.append("k")
print(listVar)
(listVar.remove("c"))
print(listVar)
listVar[3] = "Changed"
print(listVar)
###Dictionary
print("Dictionaries:")
dictionaryVar = {"a":1.0, "b":2.0, 2:3} #Mapping of keys to values (Keys and values can be of any type)
print(dictionaryVar.keys()) #Outputs a list of the keys
print(dictionaryVar.values()) #Outputs a list of the values
print(dictionaryVar.items()) #Outputs a list of tuples of the key/value pairs
dictionaryVar[1] = 3.0 #Creates a new pair with key as 1 and value as 3.0
dictionaryVar["a"] = "Pie" #Modifies key "a" to correspond to "Pie"
print(dictionaryVar.items())
###Caution
"""Using heterogenous data types tends to be a bad idea, as it will be difficult to determine
what you are operating on once you attempt to use these structures."""
###len() function on structures
print("len()")
print("Length of string: ", len(stringVar))
print("Length of tuple: ", len(tupleVar))
print("Length of list: ", len(listVar))
print("Length of dictionary: ", len(dictionaryVar))
"""len() is a function that is considered to be polymorphic.  Polymorphism means
the same function can take in different types and operate on them a certain way.
We will talk about how this works in a later episode."""
print("Length of integer: ", len(integerVar))
print("Length of float: ", len(floatVar))
"""The above error out as there is no defined length of a number.  When we take
a look at the error, notice the same key points I mentioned last time.
Important notes:
- Traceback indicates the start of a failure
- As programs can span multiple files, the file name is shown
- The line number is shown
- The faulting line is shown
- The type of error is shown along with a description of the specific cause of this
particular error based upon context (may not always make sense)"""